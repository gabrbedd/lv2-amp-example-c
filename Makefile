CFLAGS = -Wall -I../../../include

all: amp.so

amp.so: amp.o
	$(LD) amp.o -o amp.so -shared

clean:
	rm -f *.o amp.so
